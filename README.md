# estdgrid
ESTD grids on sphere.


The quadrature rules on a sphere based on efficient spherical t-design (ESTD)
taken from the website of Rob Womersley
https://web.maths.unsw.edu.au/~rsw/Sphere/EffSphDes/index.html

reference:
R. S. Womersley, Efficient Spherical Designs with Good Geometric Properties.
In: Dick J., Kuo F., Wozniakowski H. (eds) Contemporary Computational Mathematics
A Celebration of the 80th Birthday of Ian Sloan. Springer (2018) pp. 1243-1285

I didn't tabulate all the grids or the file could be huge.
I have the grids listed here


| degree | ngrids |
| ------ | ------ |
| 9 | 50 |
| ... | ... |
| 50 | 1302 |
| 60 | 1862 |
| 80 | 3282 |
| ... | ... |
| 180 | 16382 |
# usage
```
real*8 :: x(xxxxx), y(xxxxx), z(xxxxx), w(xxxxx)
call estdxxxxx ( x, y, z, w)
```
The output are the double precision arrays with the length of xxxxx. The x, y, and z are the arrays for the coordinates of points on the unit sphere. The w is the array of the corrspondent weight for these points.
